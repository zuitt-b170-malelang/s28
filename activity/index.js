// GET Method
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response)=> response.json())
// map method
.then((array)=> array.map((val)=>[val.title]))
.then((value)=> console.log(value));

// GET - single to do list item
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response)=> response.json())
.then((json)=> console.log(`The item ${json[0].title} on the list has a status of ${json[0].completed}`));

// POST Method
fetch("https://jsonplaceholder.typicode.com/todos", {
  method: "POST",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    "completed": false,
    "title": "Created To Do List Item",
    "userId": 1
  })
})
.then((response) => response.json())
.then((json)=> console.log(json));

// PUT Method
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    "title": "Updated to Do List Item",
    "description": "To update the my to do list with a different data structure",
    "status": "Pending",
    "dateCompleted": "Pending",
    "userId": 1
  })
})
.then((response) => response.json())
.then((json)=> console.log(json));

// FETCH Method
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PATCH",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    "status": "Complete",
    "dateCompleted": "07/09/21"
  })
})
.then((response) => response.json())
.then((json)=> console.log(json));

// DELETE
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "DELETE"
});